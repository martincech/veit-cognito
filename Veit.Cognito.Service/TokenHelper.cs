﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using NLog;

namespace Veit.Cognito.Service
{
    public static class TokenHelper
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private const string EMAIL_CLAIM = "email";


        public static string AccessTokenKey { get; } = "access_token";
        public static string IdTokenKey { get; } = "id_token";
        public static string RefreshTokenKey { get; } = "refresh_token";
        public static string CodeKey { get; } = "code";


        public static string ParseToken(Dictionary<string, string> tokens, string key)
        {
            if (!tokens.TryGetValue(key, out var value))
            {
                logger.Warn($"Cannot get token: {key} from collection of keys: ({string.Join(";", tokens.Keys)})");
            }
            return value;
        }

        public static string GetEmailFromToken(string token)
        {
            return GetTokenClaim(token, EMAIL_CLAIM);
        }

        public static string GetTokenClaim(string token, string claim)
        {
            var handler = new JwtSecurityTokenHandler();
            var jwtToken = handler.ReadJwtToken(token);
            var param = jwtToken.Claims.FirstOrDefault(f => f.Type.Equals(claim, StringComparison.Ordinal));
            if (param != null)
            {
                return param.Value;
            }
            return string.Empty;
        }
    }
}
