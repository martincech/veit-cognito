﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;

namespace Veit.Cognito.Service
{
    public static class CognitoService
    {
        private static readonly Logger LOG = LogManager.GetCurrentClassLogger();
        private static readonly TimeSpan clientTimeout = TimeSpan.FromSeconds(60);
        private const long RESPONSE_BUFFER_SIZE = 1024 * 1024 * 10; // 10 MB

        public static async Task<Dictionary<string, string>> Oauth2GetTokensAsync(this CognitoSettings settings, string code)
        {
            if (string.IsNullOrEmpty(code)) return new Dictionary<string, string>();
            if (settings == null) throw new ArgumentNullException(nameof(settings));
            if (string.IsNullOrEmpty(settings.CallbackSite)) throw new ArgumentException(nameof(settings.CallbackSite));
            if (string.IsNullOrEmpty(settings.ClientId)) throw new ArgumentException(nameof(settings.ClientId));

            return await Oauth2GetTokensInternalAsync(settings, code);
        }

        private static async Task<Dictionary<string, string>> Oauth2GetTokensInternalAsync(this CognitoSettings settings, string code)
        {
            var body = new Dictionary<string, string>
            {
              { "grant_type", "authorization_code" },
#pragma warning disable CC0021 // Use nameof
              { "code", code },
#pragma warning restore CC0021 // Use nameof
              { "redirect_uri", settings.CallbackSite + settings.CallbackPath},
              { "client_id", settings.ClientId }
            };
            if (!string.IsNullOrEmpty(settings.ClientSecret)) body.Add("client_secret", settings.ClientSecret);
            return await SendToTokenEndpointAsync(settings.TokenEndpoint, body);
        }


        public static async Task<Dictionary<string, string>> Oauth2RefreshTokensAsync(this CognitoSettings settings, string refreshToken)
        {
            if (string.IsNullOrEmpty(refreshToken)) return new Dictionary<string, string>();
            if (settings == null) throw new ArgumentNullException(nameof(settings));
            if (string.IsNullOrEmpty(settings.ClientId)) throw new ArgumentException(nameof(settings.ClientId));

            return await Oauth2RefreshTokensInternalAsync(settings, refreshToken);
        }

        private static async Task<Dictionary<string, string>> Oauth2RefreshTokensInternalAsync(this CognitoSettings settings, string refreshToken)
        {
            var body = new Dictionary<string, string> {
               {"client_id", settings.ClientId },
               {"refresh_token", refreshToken},
               {"grant_type", "refresh_token" }
            };
            if (!string.IsNullOrEmpty(settings.ClientSecret)) body.Add("client_secret", settings.ClientSecret);
            return await SendToTokenEndpointAsync(settings.TokenEndpoint, body);
        }


        public static async Task<Dictionary<string, string>> SendToTokenEndpointAsync(string tokenEndpoint, Dictionary<string, string> body)
        {
            if (string.IsNullOrEmpty(tokenEndpoint)) throw new ArgumentException($"{nameof(tokenEndpoint)} is NULL or empty");
            var result = "";
            try
            {
                using (var client = new HttpClient { Timeout = clientTimeout, MaxResponseContentBufferSize = RESPONSE_BUFFER_SIZE })
                {
                    result = await SendToTokenEndpointInternalAsync(client, tokenEndpoint, body);
                    return JObject.Parse(result).ToObject<Dictionary<string, string>>();
                }
            }
            catch (Exception e)
            {
                LOG.Debug(e, $"Failed to get tokens. Response : {result}");
                return new Dictionary<string, string>();
            }
        }

        private static async Task<string> SendToTokenEndpointInternalAsync(HttpClient client, string tokenEndpoint, Dictionary<string, string> body)
        {
            using (var formUrlEncodedContent = new FormUrlEncodedContent(body))
            {
                var tokenResponse = await client.PostAsync(tokenEndpoint, formUrlEncodedContent);
                var result = await tokenResponse.Content.ReadAsStringAsync();
                tokenResponse.EnsureSuccessStatusCode();
                return result;
            }
        }
    }
}
