﻿using System.Runtime.Serialization;

namespace Veit.Cognito.Service
{
    [DataContract]
    public class CognitoSettings
    {
        [DataMember]
        public string Domain { get; set; }
        [DataMember]
        public string Region { get; set; }
        [DataMember]
        public string UserPoolId { get; set; }
        [DataMember]
        public string ClientId { get; set; }
        [DataMember]
        public string ClientSecret { get; set; }
        [DataMember]
        public string CallbackPath { get; set; }
        [DataMember]
        public string CallbackSite { get; set; }
        [DataMember]
        public string Scopes { get; set; }


        [IgnoreDataMember]
        public string Issuer => $"https://cognito-idp.{Region}.amazonaws.com/{UserPoolId}";
        [IgnoreDataMember]
        public string TokenEndpoint => $"https://{Domain}.auth.{Region}.amazoncognito.com/oauth2/token";
        [IgnoreDataMember]
        public string LoginAddress => AccoutUrl("login");
        [IgnoreDataMember]
        public string RegisterAddress => AccoutUrl("signup");
        [IgnoreDataMember]
        public string LogoutAddress => $"https://{Domain}.auth.{Region}.amazoncognito.com/logout?"
           + $"client_id={ClientId}&logout_uri={CallbackSite}/";

        private string AccoutUrl(string path) => $"https://{Domain}.auth.{Region}.amazoncognito.com/{path}?"
           + "response_type=code&"
           + $"client_id={ClientId}&"
           + $"redirect_uri={CallbackSite}{CallbackPath}&"
           + $"scope={string.Join("+", (Scopes ?? "").Split(' '))}";
    }
}
