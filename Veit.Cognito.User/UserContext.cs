﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using NLog;
using Veit.Cognito.User.Map;
using Veit.Cognito.User.Model;

namespace Veit.Cognito.User
{
    public sealed class UserContext : IUserContext, IDisposable
    {
        #region Private fields

        private readonly IAmazonCognitoIdentityProvider client;
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly AwsSettings option;

        #endregion

        public UserContext(AwsSettings option)
        {
            this.option = option ?? throw new ArgumentNullException(nameof(option));
            client = new AmazonCognitoIdentityProviderClient(option.Id, option.Secret, RegionEndpoint.GetBySystemName(option.Region));
        }


        #region GET

        public async Task<Model.CognitoUser> GetCognitoUserAsync(Guid id)
        {
            try
            {
                var user = await GetUserAsync(id);
                return user.MapTo();
            }
            catch (Exception e)
            {
                logger.Error(e, "Get Cognito User Error!");
                return null;
            }
        }

        public async Task<IEnumerable<Model.CognitoUser>> GetCognitoUserAsync(IEnumerable<Guid> ids)
        {
            try
            {
                if (ids == null || !ids.Any())
                {
                    return null;
                }

                var users = await GetAllUsersAsync();
                var mappedUsers = users.MapTo();
                var filteredUsers = mappedUsers.Where(w => ids.Contains(w.Id));
                return filteredUsers;
            }
            catch (Exception e)
            {
                logger.Error(e, "Get Cognito Users Error!");
                return null;
            }
        }

        private async Task<IEnumerable<UserType>> GetAllUsersAsync()
        {
            var response = await client.ListUsersAsync(new ListUsersRequest
            {
                UserPoolId = option.UserPoolId,
                AttributesToGet = MapUser.UsedAttributes
            });
            return response.Users;
        }

        private async Task<UserType> GetUserAsync(Guid userId)
        {
            var response = await client.ListUsersAsync(new ListUsersRequest
            {
                UserPoolId = option.UserPoolId,
                Filter = $"{UserAttributes.Id}=\"{userId}\"",
                Limit = 1,
                AttributesToGet = MapUser.UsedAttributes
            });
            return response.Users.FirstOrDefault();
        }


        public async Task<bool> ExistUserAsync(string email)
        {
            try
            {
                return await ExistAsync(email);
            }
            catch (Exception e)
            {
                logger.Error(e, "Exist cognito user error");
                return false;
            }
        }

        private async Task<bool> ExistAsync(string email)
        {
            var response = await client.ListUsersAsync(new ListUsersRequest
            {
                UserPoolId = option.UserPoolId,
                Filter = $"{UserAttributes.Email}=\"{email}\"",
                Limit = 1,
                AttributesToGet = MapUser.UsedAttributes
            });
            return response.Users.Any();
        }

        #endregion

        #region Update

        public async Task<bool> UpdateUserAsync(Model.CognitoUser user)
        {
            try
            {
                return await UpdateCognitoUserAsync(user);
            }
            catch (Exception e)
            {
                logger.Error(e, "Update cognito user error");
                return false;
            }
        }

        private async Task<bool> UpdateCognitoUserAsync(Model.CognitoUser user)
        {
            var request = new AdminUpdateUserAttributesRequest
            {
                UserPoolId = option.UserPoolId,
                Username = user.Id.ToString(),
                UserAttributes = new List<AttributeType>
                {   //only name can be modified
                    new AttributeType { Name = UserAttributes.Name, Value = user.Name }
                }
            };
            var result = await client.AdminUpdateUserAttributesAsync(request);
            return result.HttpStatusCode == System.Net.HttpStatusCode.OK;
        }

        #endregion

        #region Create

        /// <summary>
        /// Create user in cognito. If success return
        /// its ID, otherwise return null.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="sendInvitation"></param>
        /// <returns></returns>
        public async Task<Guid?> CreateUserAsync(CognitoUser user, bool sendInvitation = true)
        {
            try
            {
                var response = await CreateCognitoUserAsync(user, sendInvitation);
                user = response.MapTo();
                return user?.Id;
            }
            catch (Exception e)
            {
                logger.Error(e, "Create cognito user error");
                return null;
            }
        }

        private async Task<UserType> CreateCognitoUserAsync(CognitoUser user, bool sendInvitation)
        {
            var name = string.IsNullOrEmpty(user.Name) ? user.Email : user.Name;
            var request = new AdminCreateUserRequest
            {
                UserPoolId = option.UserPoolId,
                Username = user.Email,
                MessageAction = sendInvitation ? null : MessageActionType.SUPPRESS,
                UserAttributes = new List<AttributeType>
                {
                    new AttributeType { Name = UserAttributes.Name, Value = name },
                    new AttributeType { Name = UserAttributes.Email, Value = user.Email },
                    new AttributeType { Name = UserAttributes.EmailVerified, Value = "true" },
                }
            };
            var result = await client.AdminCreateUserAsync(request);
            return result.HttpStatusCode == System.Net.HttpStatusCode.OK ? result.User : null;
        }

        /// <summary>
        /// Reset user password.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<bool> ResetUserPasswordAsync(Guid userId)
        {
            try
            {
                // Ensure that user account is not in status "Enabled / FORCE_CHANGE_PASSWORD"
                // by setting password to random value before reset.
                if (await SetCognitoUserPasswordAsync(userId, $"TMP-{Guid.NewGuid()}", true))
                {
                    return await ResetCognitoUserPasswordAsync(userId);
                }
                logger.Error("Failed to set cognito user password");
            }
            catch (Exception e)
            {
                logger.Error(e, "Reset cognito user password error");
            }
            return false;
        }

        private async Task<bool> SetCognitoUserPasswordAsync(Guid userId, string password, bool permanent)
        {
            var request = new AdminSetUserPasswordRequest
            {
                UserPoolId = option.UserPoolId,
                Username = userId.ToString(),
                Password = password,
                Permanent = permanent,
            };
            var result = await client.AdminSetUserPasswordAsync(request);
            return result.HttpStatusCode == System.Net.HttpStatusCode.OK;
        }

        private async Task<bool> ResetCognitoUserPasswordAsync(Guid userId)
        {
            var request = new AdminResetUserPasswordRequest
            {
                UserPoolId = option.UserPoolId,
                Username = userId.ToString(),
            };
            var result = await client.AdminResetUserPasswordAsync(request);
            return result.HttpStatusCode == System.Net.HttpStatusCode.OK;
        }

        #endregion

        #region Delete

        public async Task<bool> DeleteUserAsync(Guid userId)
        {
            try
            {
                return await DeleteCognitoUserAsync(userId);
            }
            catch (UserNotFoundException)
            {
                return true;
            }
            catch (Exception e)
            {
                logger.Error(e, "Delete cognito user error");
                return false;
            }
        }

        private async Task<bool> DeleteCognitoUserAsync(Guid userId)
        {
            var request = new AdminDeleteUserRequest
            {
                UserPoolId = option.UserPoolId,
                Username = userId.ToString()
            };
            var result = await client.AdminDeleteUserAsync(request);
            return result.HttpStatusCode == System.Net.HttpStatusCode.OK;
        }

        public void Dispose()
        {
            client?.Dispose();
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
