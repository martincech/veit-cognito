﻿namespace Veit.Cognito.User
{
    public static class UserAttributes
    {
        public static string Id => "sub";
        public static string Name => "name";
        public static string Email => "email";
        public static string EmailVerified => "email_verified";
        public static string Phone => "phone_number";
        public static string Note => "custom:note";
    }
}
