﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Amazon.CognitoIdentityProvider.Model;

namespace Veit.Cognito.User.Map
{
    public static class MapUser
    {
        public static List<string> UsedAttributes
        {
            get
            {
                return new List<string>
                {
                    UserAttributes.Id,
                    UserAttributes.Name,
                    UserAttributes.Email
                };
            }
        }

        public static IEnumerable<Model.CognitoUser> MapTo(this IEnumerable<UserType> users)
        {
            return users?.Select(MapTo);
        }

        public static Model.CognitoUser MapTo(this UserType user)
        {
            if (user == null)
            {
                return null;
            }
            return user.Attributes.MapTo();
        }

        private static Model.CognitoUser MapTo(this List<AttributeType> attributes)
        {
            return new Model.CognitoUser
            {
                Id = attributes.MapAttribute<Guid>(UserAttributes.Id),
                Name = attributes.MapAttribute<string>(UserAttributes.Name),
                Email = attributes.MapAttribute<string>(UserAttributes.Email)
            };
        }

        private static T MapAttribute<T>(this List<AttributeType> attributes, string attributeName)
        {
            if (attributes == null || !attributes.Any())
            {
                return default(T);
            }
            var attribute = attributes.FirstOrDefault(f => f.Name == attributeName);
            if (attribute == null)
            {
                return default(T);
            }

            return ConvertValue<T>(attribute.Value);
        }

        private static T ConvertValue<T>(string value)
        {
            return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromInvariantString(value);
        }
    }
}
