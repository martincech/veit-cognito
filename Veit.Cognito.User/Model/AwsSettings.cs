﻿namespace Veit.Cognito.User.Model
{
    public class AwsSettings : AwsIam
    {
        public string UserPoolId { get; set; }
        public string Region { get; set; }
    }
}
