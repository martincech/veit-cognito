﻿using System;

namespace Veit.Cognito.User.Model
{
    public class CognitoUser
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
