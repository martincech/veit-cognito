﻿namespace Veit.Cognito.User.Model
{
    public class AwsIam
    {
        public string Id { get; set; }
        public string Secret { get; set; }
    }
}
