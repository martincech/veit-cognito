﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Veit.Cognito.User
{
    public interface IUserContext
    {
        Task<Model.CognitoUser> GetCognitoUserAsync(Guid id);
        Task<IEnumerable<Model.CognitoUser>> GetCognitoUserAsync(IEnumerable<Guid> ids);
        Task<bool> ExistUserAsync(string email);
        Task<bool> UpdateUserAsync(Model.CognitoUser user);
        Task<Guid?> CreateUserAsync(Model.CognitoUser user, bool sendInvitation = true);
        Task<bool> ResetUserPasswordAsync(Guid userId);
        Task<bool> DeleteUserAsync(Guid userId);
    }
}
